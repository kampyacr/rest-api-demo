/**
 * 
 */
package com.gitlab.kampyacr.demo.rest_api.customer.infraestructure.adapter.in.web;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.gitlab.kampyacr.demo.api.CustomersApi;
import com.gitlab.kampyacr.demo.model.Customer;
import com.gitlab.kampyacr.demo.model.CustomerRequest;

import jakarta.validation.Valid;

/**
 * 
 */
@RestController
public class CustomerController implements CustomersApi {

    protected static final List<Customer> CUSTOMERS = new ArrayList<>(
            List.of(new Customer().id(UUID.randomUUID()).name("Pepe").lastname("Repe").address("Madrid"),
                    new Customer().id(UUID.randomUUID()).name("Maria").lastname("De la o").address("Madrid")));

    @Override
    public ResponseEntity<List<Customer>> findAll() {
        return ResponseEntity.ok(CUSTOMERS);
    }

    @Override
    public ResponseEntity<Void> addCustomer(@Valid CustomerRequest customerRequest) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerRequest, customer);
        customer.setId(UUID.randomUUID());

        CUSTOMERS.add(customer);

        HttpHeaders headers = new HttpHeaders();

        headers.setLocation(
                MvcUriComponentsBuilder.fromMethodName(CustomerController.class, "getCustomerById", customer.getId())
                        .buildAndExpand(customer.getId()).toUri());

        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<Customer> getCustomerById(UUID customerId) {
        Customer customer = CUSTOMERS.stream().filter(c -> c.getId().equals(customerId)).findFirst()
                .orElseThrow(RuntimeException::new);

        return ResponseEntity.ok(customer);
    }

    @Override
    public ResponseEntity<Customer> updateCustomer(UUID customerId, @Valid CustomerRequest customerRequest) {
        Customer customer = CUSTOMERS.stream().filter(c -> c.getId().equals(customerId)).findFirst()
                .orElseThrow(RuntimeException::new);
        BeanUtils.copyProperties(customerRequest, customer);

        return ResponseEntity.ok(customer);
    }

    @Override
    public ResponseEntity<Void> deleteCustomer(UUID customerId) {
        Customer customer = CUSTOMERS.stream().filter(c -> c.getId().equals(customerId)).findFirst()
                .orElseThrow(RuntimeException::new);

        CUSTOMERS.remove(customer);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
