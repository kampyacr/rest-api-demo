package com.gitlab.kampyacr.demo.rest_api.customer.infraestructure.adapter.in.web;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeTypeUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.kampyacr.demo.api.CustomersApi;
import com.gitlab.kampyacr.demo.model.Customer;
import com.gitlab.kampyacr.demo.model.CustomerRequest;

//@WebMvcTest(controllers = CustomerController.class)
public class CustomerControllerTest {

//    @Autowired
    private MockMvc mockMvc;

    private CustomersApi customersApi = new CustomerController();

//    private GetCustomerUseCase getCustomerUseCase; 
    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(customersApi).build();
    }

    @AfterEach
    public void tearDown() {
        mockMvc = null;
    }

    @Test
    void when_get_all_customer_should_return_ok() throws Exception {

        // Given

        // When
        final ResultActions result = mockMvc.perform(get("/customers").accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        // Then
        final int expectedSize = CustomerController.CUSTOMERS.size();
        final String[] expectedCustomerNames = CustomerController.CUSTOMERS.stream().map(Customer::getName)
                .collect(Collectors.toList()).toArray(new String[CustomerController.CUSTOMERS.size()]);

        result.andExpect(content().contentType(MimeTypeUtils.APPLICATION_JSON_VALUE));
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.length()").value(expectedSize));
        result.andExpect(jsonPath("$[*].name", containsInAnyOrder(expectedCustomerNames)));
    }

    @Test
    void when_create_a_customer_return_created() throws Exception {
        // Given
        CustomerRequest customerRequest = new CustomerRequest();
        customerRequest.name("Maria").lastname("Duke").address("Barcelona");

        // When
        final ResultActions result = mockMvc.perform(post("/customers").content(asJsonString(customerRequest))
                .contentType(MimeTypeUtils.APPLICATION_JSON_VALUE).accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        // Then
        result.andExpect(status().isCreated());
        result.andExpect(header().exists("Location"));

    }

    @Test
    void when_get_one_cutomer_should_custumer() throws Exception {
        // Given
        UUID customerId = CustomerController.CUSTOMERS.get(0).getId();

        // When
        final ResultActions result = mockMvc
                .perform(get("/customers/{customerId}", customerId).accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").value(customerId.toString()));
        result.andExpect(content().contentType(MimeTypeUtils.APPLICATION_JSON_VALUE));

    }

    @Test
    void when_update_customer_should_customer_updated() throws Exception {
        // Given
        UUID customerId = CustomerController.CUSTOMERS.get(0).getId();
        CustomerRequest customerRequest = new CustomerRequest();
        customerRequest.name("Maria").lastname("Duke").address("Barcelona");

        // When
        final ResultActions result = mockMvc.perform(put("/customers/{customerId}", customerId)
                .content(asJsonString(customerRequest)).contentType(MimeTypeUtils.APPLICATION_JSON_VALUE)
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        // Then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.name").value("Maria"));
        result.andExpect(jsonPath("$.lastname").value("Duke"));
        result.andExpect(jsonPath("$.address").value("Barcelona"));
        result.andExpect(content().contentType(MimeTypeUtils.APPLICATION_JSON_VALUE));
    }

    @Test
    void when_delete_customer_should_return_not_content() throws Exception {
        // Given
        UUID customerId = CustomerController.CUSTOMERS.get(0).getId();

        // When
        final ResultActions result = mockMvc
                .perform(delete("/customers/{customerId}", customerId).accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        // Then
        result.andExpect(status().isNoContent());

    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
